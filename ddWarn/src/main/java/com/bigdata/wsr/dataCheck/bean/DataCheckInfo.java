package com.bigdata.wsr.dataCheck.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataCheckInfo {
    private String dbName;
    private String tableName;
    private int mysqlNum;
    private int dorisNum;
    private int deltaNum;
}
