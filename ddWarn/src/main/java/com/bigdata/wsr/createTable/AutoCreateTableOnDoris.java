package com.bigdata.wsr.createTable;


import com.bigdata.wsr.createTable.util.AutoCreateTableUtil;
import lombok.extern.slf4j.Slf4j;

/**
 * 自动创建表
 *
 * @author rui.wang
 * @date 2022/11/14
 */
@Slf4j
public class AutoCreateTableOnDoris {
    public static void main(String[] args) {
        AutoCreateTableUtil.AutoCreateTableOnAccount();
        AutoCreateTableUtil.AutoCreateTableOnBigFrog();
        AutoCreateTableUtil.AutoCreateTableOnBi();
        AutoCreateTableUtil.AutoCreateTableOnProof();
    }
}
