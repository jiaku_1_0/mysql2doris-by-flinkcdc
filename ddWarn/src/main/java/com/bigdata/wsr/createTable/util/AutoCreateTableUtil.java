package com.bigdata.wsr.createTable.util;

import com.alibaba.druid.pool.DruidDataSource;
import com.bigdata.wsr.createTable.bean.FieldInfo;
import com.bigdata.wsr.createTable.config.DbConfig;
import com.bigdata.wsr.createTable.sink.DorisDdlUtil;
import com.bigdata.wsr.createTable.source.SourceTableUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AutoCreateTableUtil {
    public static void AutoCreateTableOnAccount() {
        // MySQL 数据源
        DruidDataSource ds = DbConfig.getMysql137AccountDs();
        // doris 数据源
        DruidDataSource dorisDs = DbConfig.getDorisAccountDs();
        // 待创建的表
        List<String> tableList = new ArrayList<>();
        tableList.add("refund_info");
        tableList.add("tri_abate_info");

        for (String tableName : tableList) {
            //todo: 1、查询 mysql表数据
            String ddl = SourceTableUtil.getTableDdl(tableName, ds);
            int columnCount = SourceTableUtil.getColumnCount(tableName, ds);

            //todo：2、解析ddl
            List<FieldInfo> fieldInfoList = new ArrayList<>();
            fieldInfoList.add(
                    new FieldInfo(
                            "is_delete_doris",
                            "varchar(4)",
                            "逻辑删除标志位，1：逻辑删除"
                    )
            );
            Map<String, String> keyFieldMap = new HashMap<>();
            SourceTableUtil.ddlParseToField(ddl, columnCount, fieldInfoList, keyFieldMap);

            //todo：3、适配doris中的字段类型、字段顺序及长度
            List<FieldInfo> dorisFieldSortedInfoList = new ArrayList<>();
            DorisDdlUtil.dorisAdaptor(fieldInfoList, keyFieldMap, dorisFieldSortedInfoList);

            //todo：4、组装成 doris 建表语句。
            String dorisDdl = DorisDdlUtil.getDorisDdl("account." + tableName, dorisFieldSortedInfoList, keyFieldMap);
//            System.out.println(dorisDdl);

            //todo：5、连接 doris 进行建表。
            DorisDdlUtil.autoCreateDorisTable(dorisDs, tableName, dorisDdl);
        }
    }

    public static void AutoCreateTableOnBigFrog() {
        // MySQL 数据源
        DruidDataSource ds = DbConfig.getMysql137BigFrogDs();
        // doris 数据源
        DruidDataSource dorisDs = DbConfig.getDorisBigFrogDs();
        // 待创建的表
        List<String> tableList = new ArrayList<>();
        tableList.add("clms_fee_package");
        tableList.add("clms_fee_pay_apply_bill");
        tableList.add("clms_fee_pay_apply_bill_fee");
        tableList.add("clms_product");
        tableList.add("ofs_make_loan");
        tableList.add("ofs_receipt_busi_detail");

        for (String tableName : tableList) {
            //todo: 1、查询 mysql表数据
            String ddl = SourceTableUtil.getTableDdl(tableName, ds);
            int columnCount = SourceTableUtil.getColumnCount(tableName, ds);

            //todo：2、解析ddl
            List<FieldInfo> fieldInfoList = new ArrayList<>();
            fieldInfoList.add(
                    new FieldInfo(
                            "is_delete_doris",
                            "varchar(4)",
                            "逻辑删除标志位，1：逻辑删除"
                    )
            );
            Map<String, String> keyFieldMap = new HashMap<>();
            SourceTableUtil.ddlParseToField(ddl, columnCount, fieldInfoList, keyFieldMap);

            //todo：3、适配doris中的字段类型、字段顺序及长度
            List<FieldInfo> dorisFieldSortedInfoList = new ArrayList<>();
            DorisDdlUtil.dorisAdaptor(fieldInfoList, keyFieldMap, dorisFieldSortedInfoList);

            //todo：4、组装成 doris 建表语句。
            String dorisDdl = DorisDdlUtil.getDorisDdl(tableName, dorisFieldSortedInfoList, keyFieldMap);
//            System.out.println(dorisDdl);

            //todo：5、连接 doris 进行建表。
            DorisDdlUtil.autoCreateDorisTable(dorisDs, tableName, dorisDdl);
        }
    }

    public static void AutoCreateTableOnBi() {
        // MySQL 数据源
        DruidDataSource ds = DbConfig.getMysql137BiDs();
        // doris 数据源
        DruidDataSource dorisDs = DbConfig.getDorisBiDs();
        // 待创建的表
        List<String> tableList = new ArrayList<>();
        tableList.add("aepayslip_rate2");
        tableList.add("bi_01_risk_pro");
        tableList.add("risk_customer_giftamt_detail");
        tableList.add("riskexpert_bill_month");
        tableList.add("saasfee_input");

        for (String tableName : tableList) {
            //todo: 1、查询 mysql表数据
            String ddl = SourceTableUtil.getTableDdl(tableName, ds);
            int columnCount = SourceTableUtil.getColumnCount(tableName, ds);

            //todo：2、解析ddl
            List<FieldInfo> fieldInfoList = new ArrayList<>();
            fieldInfoList.add(
                    new FieldInfo(
                            "is_delete_doris",
                            "varchar(4)",
                            "逻辑删除标志位，1：逻辑删除"
                    )
            );
            Map<String, String> keyFieldMap = new HashMap<>();
            SourceTableUtil.ddlParseToField(ddl, columnCount, fieldInfoList, keyFieldMap);

            //todo：3、适配doris中的字段类型、字段顺序及长度
            List<FieldInfo> dorisFieldSortedInfoList = new ArrayList<>();
            DorisDdlUtil.dorisAdaptor(fieldInfoList, keyFieldMap, dorisFieldSortedInfoList);

            //todo：4、组装成 doris 建表语句。
            String dorisDdl = DorisDdlUtil.getDorisDdl(tableName, dorisFieldSortedInfoList, keyFieldMap);
//            System.out.println(dorisDdl);

            //todo：5、连接 doris 进行建表。
            DorisDdlUtil.autoCreateDorisTable(dorisDs, tableName, dorisDdl);
        }
    }

    public static void AutoCreateTableOnProof() {
        // MySQL 数据源
        DruidDataSource ds = DbConfig.getMysql137ProofDs();
        // doris 数据源
        DruidDataSource dorisDs = DbConfig.getDorisProofDs();
        // 待创建的表
        List<String> tableList = new ArrayList<>();
        tableList.add("contract_solution");

        for (String tableName : tableList) {
            //todo: 1、查询 mysql表数据
            String ddl = SourceTableUtil.getTableDdl(tableName, ds);
            int columnCount = SourceTableUtil.getColumnCount(tableName, ds);

            //todo：2、解析ddl
            List<FieldInfo> fieldInfoList = new ArrayList<>();
            fieldInfoList.add(
                    new FieldInfo(
                            "is_delete_doris",
                            "varchar(4)",
                            "逻辑删除标志位，1：逻辑删除"
                    )
            );
            Map<String, String> keyFieldMap = new HashMap<>();
            SourceTableUtil.ddlParseToField(ddl, columnCount, fieldInfoList, keyFieldMap);

            //todo：3、适配doris中的字段类型、字段顺序及长度
            List<FieldInfo> dorisFieldSortedInfoList = new ArrayList<>();
            DorisDdlUtil.dorisAdaptor(fieldInfoList, keyFieldMap, dorisFieldSortedInfoList);

            //todo：4、组装成 doris 建表语句。
            String dorisDdl = DorisDdlUtil.getDorisDdl(tableName, dorisFieldSortedInfoList, keyFieldMap);
//            System.out.println(dorisDdl);

            //todo：5、连接 doris 进行建表。
            DorisDdlUtil.autoCreateDorisTable(dorisDs, tableName, dorisDdl);
        }
    }
}
