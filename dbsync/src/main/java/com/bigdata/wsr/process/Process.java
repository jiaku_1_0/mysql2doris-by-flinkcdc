package com.bigdata.wsr.process;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

public interface Process {
    void process(StreamExecutionEnvironment env, String ServerId, int parallelism);
}
