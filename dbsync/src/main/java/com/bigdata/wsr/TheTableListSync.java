package com.bigdata.wsr;

import com.bigdata.wsr.task.AccountTask;
import com.bigdata.wsr.task.BiTask;
import com.bigdata.wsr.task.BigfrogTask;
import com.bigdata.wsr.task.ProofTask;
import lombok.extern.slf4j.Slf4j;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;

/**
 * mysql ---> doris 整库同步。
 *
 * <p>
 * FQA：https://github.com/ververica/flink-cdc-connectors/wiki/FAQ(ZH)
 *
 * <p>
 * serverId 分配：
 * account      ---> 5400   ---> 1
 * bigfrog      ---> 5401   ---> 1
 * bi           ---> 5402   ---> 1
 * proof        ---> 5403   ---> 1
 *
 * <p>
 * 码云链接：https://gitee.com/wsrarea/mysql2doris-by-flinkcdc
 *
 * @author rui.wang
 * @date 2022/11/07
 */

@Slf4j
public class TheTableListSync {
    public static void main(String[] args) throws Exception {
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        // enable checkpoint
        env.enableCheckpointing(10000);

        //todo：1、account 库同步
        new AccountTask().process(env, "5400", 1);//serverId个数要与source并行度保持一致

        //todo：2、bigfrog 库同步
        new BigfrogTask().process(env, "5401", 1);

        //todo：3、bi 库同步
        new BiTask().process(env, "5402", 1);

        //todo：4、proof 库同步
        new ProofTask().process(env, "5403", 1);

        env.execute("MySQL the tables sync to doris.");
    }
}
